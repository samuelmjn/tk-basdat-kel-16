from django import forms
from django.db import connection
import datetime
from django.core.exceptions import ValidationError

class AddForm(forms.Form):
  cursor = connection.cursor()
  cursor.execute('SELECT email, email FROM EMAIL_PELANGGAN()')
  row = cursor.fetchall()
  EMAIL_CHOICES = row

  TANGGAL_CHOICES = []

  email = forms.EmailField(max_length=50, widget=forms.Select(choices = EMAIL_CHOICES))
  tanggal = forms.DateTimeField(widget=forms.Select(choices = TANGGAL_CHOICES))
  status = forms.CharField()
  rating = forms.IntegerField()

  def save(self):
    cd = self.cleaned_data
    now = str(datetime.datetime.now())
    tge = str(cd['tanggal'])
    rt = str(cd['rating'])

    cursor = connection.cursor()

    try:
      cursor.execute(
        "INSERT INTO TESTIMONI VALUES('"
        + cd['email'] + "','"
        + tge + "','"
        + now + "','"
        + cd['status'] + "','"
        + rt + "')"
      )
    except Exception as e:
      self.add_error(None, e)

class UpdateForm(forms.Form):
  email = forms.EmailField(max_length=50, widget=forms.EmailInput(attrs={'readonly':'readonly'}))
  tanggal_transaksi = forms.DateTimeField(widget=forms.DateTimeInput(attrs={'readonly':'readonly'}))
  tanggal_testimoni = forms.DateTimeField(widget=forms.HiddenInput)
  status = forms.CharField()
  rating = forms.IntegerField()

  def save(self):
    cd = self.cleaned_data
    now = str(datetime.datetime.now())
    tt = str(cd['tanggal_transaksi'])
    tteo = str(cd['tanggal_testimoni'])
    rt = str(cd['rating'])

    cursor = connection.cursor()

    try:
      cursor.execute(
        "UPDATE TESTIMONI SET tanggal_testimoni = '" + now + "', status = '" + cd['status'] + "', rating = '" + rt
        + "' WHERE email='" + cd['email']
		    + "' AND tanggal_testimoni='" + tteo
		    + "' AND tanggal_transaksi='" + tt + "'"
      )
    except Exception as e:
      self.add_error(None, e)