from django.urls import path
from .views import *

app_name = 'testimoni'

urlpatterns = [
    path('<int:page>/', index, name='index'),
    path('other/<int:page>/', other, name='other'),
    path('add/', add, name='add'),
    path('update/<str:email>/<str:tanggal_testimoni>/<str:tanggal_transaksi>/', update, name='update'),
    path('datechoices/', datechoices, name='datechoices'),
    path('deletetestimoni/<int:page>/<str:email>/<str:tanggal_testimoni>/<str:tanggal_transaksi>/', deletetestimoni, name='deletetestimoni')
]
