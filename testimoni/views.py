from django.shortcuts import render
from .forms import *
from django.db import connection
from django.shortcuts import redirect
from django.http import JsonResponse, HttpResponseForbidden

datas = [
	{
		'email': 'lauren93@gmail.com',
		'tanggal_transaksi': '1949-08-28 00:00:00',
		'tanggal_testimoni': '1925-07-22 00:00:00',
		'status': 'a',
		'rating': 1
	},

	{
		'email': 'rachel77@phillips.com',
		'tanggal_transaksi': '1912-01-31 00:00:00',
		'tanggal_testimoni': '1966-05-13 00:00:00',
		'status': 'a',
		'rating': 2
	},

	{
		'email': 'ryanbailey@boyd.com',
		'tanggal_transaksi': '1936-02-03 00:00:00',
		'tanggal_testimoni': '2018-06-04 00:00:00',
		'status': 'a',
		'rating': 6
	},

	{
		'email': 'ismith@jones.com',
		'tanggal_transaksi': '1979-06-01 00:00:00',
		'tanggal_testimoni': '1928-12-03 00:00:00',
		'status': 'a',
		'rating': 7
	},

	{
		'email': 'amanda02@snow.net',
		'tanggal_transaksi': '1951-07-15 00:00:00',
		'tanggal_testimoni': '1996-11-30 00:00:00',
		'status': 'n',
		'rating': 6
	}
]


# Create your views here.
def index(request, page):
	if not request.session.get('pengguna'):
		return HttpResponseForbidden()
	elif not request.session['pengguna']['is_pelanggan']:
		return HttpResponseForbidden()

	email = request.session['pengguna']['email']
	cursor = connection.cursor()
	offset = str(page * 10)
	cursor.execute("SELECT * FROM TESTIMONI WHERE email = '" + email +"' LIMIT 10 OFFSET " + offset)
	row = cursor.fetchall()

	response = {
		'isPelanggan': True,
		'datas': row,
		'cp' : page,
		'pp' : page - 1,
		'np' : page + 1
	}

	return render(request, "testimoni/read.html", response)

def other(request, page):
	cursor = connection.cursor()
	offset = str(page * 10)
	cursor.execute("SELECT * FROM TESTIMONI LIMIT 10 OFFSET " + offset)
	row = cursor.fetchall()
	
	response = {
		'input_form': UpdateForm(),
		'isPelanggan': False,
		'datas': row,
		'cp' : page,
		'pp' : page - 1,
		'np' : page + 1
	}

	return render(request, "testimoni/read.html", response)


def add(request):
	if not request.session.get('pengguna'):
		return HttpResponseForbidden()
	elif not request.session['pengguna']['is_pelanggan']:
		return HttpResponseForbidden()

	message = ''

	if(request.method == 'POST'):
		form = AddForm(request.POST)
		if(form.is_valid()):
			form.save()
			cd = form.cleaned_data
			if(cd['email'] != request.session['pengguna']['email']):
				form.add_error('email', 'Email harus sama dengan email login')
			if not form.errors:
				message = 'Testimoni berhasil ditambahkan'
				request.COOKIES['message'] = message
				return index(request, 0)
	else:
		form = AddForm()

	response = {
		'input_form': form,
		'message' : message
	}

	return render(request, "testimoni/add.html", response)


def update(request, email, tanggal_testimoni, tanggal_transaksi):
	if not request.session.get('pengguna'):
		return HttpResponseForbidden()
	elif not request.session['pengguna']['is_pelanggan']:
		return HttpResponseForbidden()

	message = ''

	if(request.method == 'POST'):
		form = UpdateForm(request.POST)
		if(form.is_valid()):
			form.save()
			if not form.errors:
				message = 'Testimoni berhasil diupdate'
				request.COOKIES['message'] = message
				return index(request, 0)
	else:
		cursor = connection.cursor()
		cursor.execute(
			"SELECT * FROM TESTIMONI WHERE email='" + email 
			+ "' AND tanggal_testimoni='" + tanggal_testimoni 
			+ "' AND tanggal_transaksi='" + tanggal_transaksi + "'"
			)
		row = cursor.fetchone()
		form = UpdateForm(initial={
			'email' : row[0],
			'tanggal_testimoni' : row[2],
			'tanggal_transaksi' : row[1],
			'status' : row[3],
			'rating' : row[4]
		})

	response = {
		'input_form': form,
		'message' : message
	}

	return render(request, "testimoni/update.html", response)

def datechoices(request):
	email = request.GET.get('email', '')
	cursor = connection.cursor()
	cursor.execute(
		"SELECT tanggal,tanggal FROM TANGGAL_TRANSAKSI('" + email +"')"
	)
	row = cursor.fetchall()
	json = {'dates' : row}
	return JsonResponse(json)

def deletetestimoni(request, page, email, tanggal_testimoni, tanggal_transaksi):
	if not request.session.get('pengguna'):
		return HttpResponseForbidden()
	elif not request.session['pengguna']['is_pelanggan']:
		return HttpResponseForbidden()
	
	message = ''
	cursor = connection.cursor()
	try:
		cursor.execute(
			"DELETE FROM TESTIMONI WHERE email='" + email 
			+ "' AND tanggal_testimoni='" + tanggal_testimoni 
			+ "' AND tanggal_transaksi='" + tanggal_transaksi + "'"
		)
		message = 'Testimoni berhasil dihapus'
	except Exception as e:
		message = e

	request.COOKIES['message'] = message
	return index(request, page)