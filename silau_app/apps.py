from django.apps import AppConfig


class SilauAppConfig(AppConfig):
    name = 'silau_app'
