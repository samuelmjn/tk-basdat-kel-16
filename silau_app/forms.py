from django import forms
import unicodedata
from django.db import connection

from django.core.exceptions import ValidationError
from django.contrib.auth.models import User

mockDB = {
	"greerrobin@carter.com": {
		"password": "4&K5GMxo(3",
		"role": 2
	},
	"margaret73@villegas.com": {
		"password": "&zWmF59Us9",
		"role": 2
	},
	"zwallace@alexander.com": {
		"password": "X8J(4VBgYT",
		"role": 2
	},
	"markgreen@goodwin.biz": {
		"password": "0rfTUq(7_H",
		"role": 2
	},
	"malloryadams@sullivan.biz": {
		"password": "^zFO@Qo&N6",
		"role": 2
	},
	"baldwindawn@yahoo.com": {
		"password": "AZ12PsqM4$",
		"role": 2
	},
	"michaelburns@gmail.com": {
		"password": "2bGBA@DB^h",
		"role": 2
	},
	"cynthia18@hotmail.com": {
		"password": "#NAME?",
		"role": 2
	},
	"bakerrussell@yahoo.com": {
		"password": "r)0KZv*ww5",
		"role": 2
	},
	"castilloronald@hotmail.com": {
		"password": "vWgnVuM3+3",
		"role": 2
	},
	"pottserin@hotmail.com": {
		"password": "1qlJ43w+(5",
		"role": 2
	},
	"kaitlynthompson@yahoo.com": {
		"password": "G4H#lY2b%_",
		"role": 2
	},
	"ghensley@richardson-hutchinson.net": {
		"password": "a&8!EMd_8R",
		"role": 2
	},
	"mlowery@hunter-baird.com": {
		"password": "TCc#YSj!#0",
		"role": 2
	},
	"april28@snyder.info": {
		"password": "2GXtSPfA#8",
		"role": 2
	},
	"zsmith@hotmail.com": {
		"password": "@13vSaXzp@",
		"role": 2
	},
	"brianfritz@yahoo.com": {
		"password": "%93ibHgFIW",
		"role": 2
	},
	"dylanlee@gmail.com": {
		"password": "&5Qf(XeA&w",
		"role": 2
	},
	"vharper@williams-patterson.net": {
		"password": "3zKnPf)E@U",
		"role": 2
	},
	"qallen@gmail.com": {
		"password": "zx)4WZYbO+",
		"role": 2
	},
	"aclayton@yahoo.com": {
		"password": "Kb!6nNXlQ@",
		"role": 0
	},
	"kanderson@hotmail.com": {
		"password": "k*5Y6jj!8y",
		"role": 0
	},
	"deniseking@yahoo.com": {
		"password": "u*M5tv1F&8",
		"role": 0
	},
	"matthewperkins@gmail.com": {
		"password": "*nFGT8St+a",
		"role": 0
	},
	"bankspaul@briggs.com": {
		"password": "n&6hFtJS+k",
		"role": 0
	},
	"lisa41@hotmail.com": {
		"password": "@GLieTIP4g",
		"role": 0
	},
	"mclaughlindevon@nelson-vasquez.biz": {
		"password": "yh7sLyD1_Q",
		"role": 0
	},
	"cameron42@yahoo.com": {
		"password": "C6ZqOd!i*S",
		"role": 0
	},
	"smithstephanie@watts-bradley.com": {
		"password": "##F(0HCqVA",
		"role": 0
	},
	"janicetaylor@smith-green.com": {
		"password": "d()5Olkc^T",
		"role": 0
	},
	"taylorjennifer@johnson.biz": {
		"password": "&QfWa68Fs5",
		"role": 1
	},
	"simmonsrobert@hayes.net": {
		"password": "&u_qJYKoa3",
		"role": 1
	},
	"emilyramsey@hotmail.com": {
		"password": "(6M1WTPe*%",
		"role": 1
	},
	"underwoodvictoria@keith.com": {
		"password": "J0yJHfdm&T",
		"role": 1
	},
	"paynealan@yahoo.com": {
		"password": "Rh8u1RL_1*",
		"role": 1
	},
	"elizabethlewis@hotmail.com": {
		"password": "2LEgn4Ee^*",
		"role": 1
	},
	"brandon77@yahoo.com": {
		"password": "6zhVyVIQ&g",
		"role": 1
	},
	"hollandjoseph@gmail.com": {
		"password": "^w7#Fcs0@3",
		"role": 1
	},
	"lauren93@gmail.com": {
		"password": "X2S^lAFa^P",
		"role": 1
	},
	"laurenroberson@yahoo.com": {
		"password": "%Ep%Y3JmyP",
		"role": 1
	},
	"ismith@jones.com": {
		"password": "#0KgBH)w+0",
		"role": 1
	},
	"rachel77@phillips.com": {
		"password": "*@RRewuc38",
		"role": 1
	},
	"umcgee@lara.com": {
		"password": "y4Pr+KBz*5",
		"role": 1
	},
	"jacob07@yahoo.com": {
		"password": "j*H(5MZhY8",
		"role": 1
	},
	"jgriffin@jennings.com": {
		"password": "v_w8J5Rxoo",
		"role": 1
	},
	"joseph85@hotmail.com": {
		"password": "R0IYf3r__U",
		"role": 1
	},
	"davidhenderson@yahoo.com": {
		"password": "(1G7KZfy@o",
		"role": 1
	},
	"christie87@williams.net": {
		"password": "+JZCXSBb!1",
		"role": 1
	},
	"melody73@hotmail.com": {
		"password": "$AxgXuuFg4",
		"role": 1
	},
	"kim40@yahoo.com": {
		"password": "C197AzYNx$",
		"role": 1
	},
	"stephen32@cummings-melendez.com": {
		"password": "h5v2MIcuQ+",
		"role": 1
	},
	"zmccarthy@gmail.com": {
		"password": "*t1Fre!Irz",
		"role": 1
	},
	"willisdebra@lewis.biz": {
		"password": "*yy@3kXP#1",
		"role": 1
	},
	"mclark@barnes.com": {
		"password": "Mi!fm!i2*0",
		"role": 1
	},
	"clarkleah@washington.com": {
		"password": "%N0Ew2@j#+",
		"role": 1
	},
	"michaelgonzales@gutierrez-mcdaniel.com": {
		"password": "+9WIzm2Erm",
		"role": 1
	},
	"hortonrebekah@hotmail.com": {
		"password": "DD0*ZahSJ_",
		"role": 1
	},
	"msullivan@mills-everett.com": {
		"password": "_2Qp6Pmiwo",
		"role": 1
	},
	"kellygonzalez@gmail.com": {
		"password": "UH)n4Apxrp",
		"role": 1
	},
	"joshuajohnson@moore.info": {
		"password": "9eDt7Yk*^5",
		"role": 1
	},
	"thorntonzachary@hotmail.com": {
		"password": "fqk8Dy!xz@",
		"role": 1
	},
	"chuber@tucker.com": {
		"password": "!NR#DhUu2g",
		"role": 1
	},
	"sethlambert@green.info": {
		"password": "%2kNCXm2eR",
		"role": 1
	},
	"singhangela@gmail.com": {
		"password": "&024Sq7tV&",
		"role": 1
	},
	"acunningham@hotmail.com": {
		"password": "q&3U5WYr4!",
		"role": 1
	},
	"angel19@nguyen-kelly.info": {
		"password": "j2#A)RHA+V",
		"role": 1
	},
	"dmccormick@sawyer.net": {
		"password": "_oei81Ogf5",
		"role": 1
	},
	"lori54@garcia.info": {
		"password": "&A2_f3IzV5",
		"role": 1
	},
	"colin77@gmail.com": {
		"password": "a9O4ZF8y^x",
		"role": 1
	},
	"grayangela@clayton.info": {
		"password": "%1Blz!f&nx",
		"role": 1
	},
	"ryanbailey@boyd.com": {
		"password": "_iYJx_DmH5",
		"role": 1
	},
	"lisa88@gmail.com": {
		"password": ")4ZESYl+73",
		"role": 1
	},
	"amanda02@snow.net": {
		"password": "7$R4PYkmN+",
		"role": 1
	},
	"ekelly@ford.com": {
		"password": ")2eEIkgkNB",
		"role": 1
	},
	"duane01@hodges.info": {
		"password": "gq4YZ(gmd_",
		"role": 1
	},
	"jasonboyd@fields.info": {
		"password": "^%8v@HSagg",
		"role": 1
	},
	"tracy43@yahoo.com": {
		"password": "%R#1dWyuNe",
		"role": 1
	},
	"gregorysmith@yahoo.com": {
		"password": "3I9W$TVmZ*",
		"role": 1
	},
	"jackkline@gomez.com": {
		"password": "!*2hD)Xclw",
		"role": 1
	},
	"jamesvasquez@lester.info": {
		"password": "5H@43UFmR&",
		"role": 1
	},
	"mhenry@yahoo.com": {
		"password": "fZ6Ph%mr)O",
		"role": 1
	},
	"steven34@yahoo.com": {
		"password": "@_HAna^Su7",
		"role": 1
	},
	"arthurmejia@garcia.info": {
		"password": "e7)1QA0n)t",
		"role": 1
	},
	"raymond60@malone-dennis.com": {
		"password": "0)7tTl88*U",
		"role": 1
	},
	"shirleyjenkins@gmail.com": {
		"password": "_2^3hSjpm4",
		"role": 1
	},
	"dennis51@gmail.com": {
		"password": "V+8ShGyJma",
		"role": 1
	},
	"dkaufman@yahoo.com": {
		"password": "WI0BcCv6(H",
		"role": 1
	},
	"kelseyfinley@yahoo.com": {
		"password": "y_JVy3Zv^D",
		"role": 1
	},
	"rebeccaanderson@gmail.com": {
		"password": "xjw8IPfp#Q",
		"role": 1
	},
	"sherrienglish@davies.biz": {
		"password": "UWVUeqoj)9",
		"role": 1
	},
	"oedwards@yahoo.com": {
		"password": "!oUj&b(W@0",
		"role": 1
	},
	"sandracurtis@young.com": {
		"password": "4Yvx9LTe$$",
		"role": 1
	},
	"perrymichelle@cooper.info": {
		"password": "!P7tXJdd5H",
		"role": 1
	},
	"woodwanda@pierce-buckley.com": {
		"password": "%2G05+SlL1",
		"role": 1
	},
	"wbates@lewis.info": {
		"password": "t*Fv41@keR",
		"role": 1
	},
	"michael47@gmail.com": {
		"password": "s+8LymhIKc",
		"role": 1
	},
	"jennifer64@kelly-patel.info": {
		"password": "!B%2nU8lrX",
		"role": 1
	},
	"kmueller@barrett.com": {
		"password": "gOw^1DiIJv",
		"role": 1
	},
	"brittneyyork@stephenson-bailey.com": {
		"password": "%I9C_RYcAy",
		"role": 1
	},
	"jenkinsparker@yahoo.com": {
		"password": "C(84CJ)bSm",
		"role": 1
	}
}

class UsernameField(forms.CharField):
		def to_python(self, value):
				return unicodedata.normalize('NFKC', super().to_python(value))

		def widget_attrs(self, widget):
				return {
						**super().widget_attrs(widget),
						'autocapitalize': 'none',
						'autocomplete': 'username',
				}

class AuthenticationForm(forms.Form):
		"""
		Base class for authenticating users. Extend this to get a form that accepts
		username/password logins.
		"""
		username = UsernameField(widget=forms.TextInput(attrs={'autofocus': True}))
		password = forms.CharField(
				label="Password",
				strip=False,
				widget=forms.PasswordInput(attrs={'autocomplete': 'current-password'}),
		)

		error_messages = {
				'invalid_login': (
						"Please enter a correct %(username)s and password. Note that both "
						"fields may be case-sensitive."
				),
				'inactive': ("This account is inactive."),
		}

		def __init__(self, request=None, *args, **kwargs):
				"""
				The 'request' parameter is set for custom auth use by subclasses.
				The form data comes in via the standard 'data' kwarg.
				"""
				self.request = request
				self.user = None

				super().__init__(*args, **kwargs)

				# Set the max length and label for the "username" field.
				# username_max_length = self.username_field.max_length or 254
				self.fields['username'].max_length = 50
				self.fields['username'].widget.attrs['maxlength'] = 50
				# if self.fields['username'].label is None:
				#     self.fields['username'].label = capfirst(self.username_field.verbose_name)

		def clean(self):
				username = self.cleaned_data.get('username')
				password = self.cleaned_data.get('password')

				staffUser, _ = User.objects.get_or_create(username='staff', defaults={'is_staff': True, 'first_name':'Staff'})
				kurirUser, _ = User.objects.get_or_create(username='kurir', defaults={'is_staff': True, 'first_name':'Kurir'})
				normalUser, _ = User.objects.get_or_create(username='pelanggan', defaults={'is_staff': False, 'first_name':'Pelanggan'})

				if username is not None and password:
						if username in mockDB:
								if password == mockDB[username]["password"]:
									r = mockDB[username]["role"]
									if r == 0:
										self.user = staffUser
									elif r == 2:
										self.user = kurirUser
									else:
										self.user = normalUser
						if self.user is None:
								raise self.get_invalid_login_error()

				return self.cleaned_data

		def get_user(self):
				return self.user

		def get_invalid_login_error(self):
				return ValidationError(
						self.error_messages['invalid_login'],
						code='invalid_login',
						# params={'username': self.username_field.verbose_name},
				)

# Login 2
class LoginForm(forms.Form):
	email = forms.EmailField(max_length=50, required=True)
	password = forms.CharField(max_length=50, required=True, widget=forms.PasswordInput)

class RegisterStaffForm(forms.Form):
	email = forms.EmailField(max_length=50, required=True)
	password = forms.CharField(max_length=50, required=True, widget=forms.PasswordInput)
	nama_lengkap = forms.CharField(max_length=50, required=True)
	no_telepon = forms.CharField(max_length=20, required=True)
	jenis_kelamin = forms.CharField(max_length=1, required=True)
	alamat = forms.CharField(required=True)
	npwp = forms.CharField(max_length=20, required=True)
	no_rekening = forms.CharField(max_length=20, required=True)
	nama_bank = forms.CharField(max_length=50, required=True)
	kantor_cabang = forms.CharField(max_length=50, required=True)

	def save(self):
		cd = self.cleaned_data

		cursor = connection.cursor()

		try:
			cursor.execute(
				"INSERT INTO PENGGUNA VALUES('"
				+ cd['email'] + "','"
				+ cd['password'] + "','"
				+ cd['nama_lengkap']+ "','"
				+ cd['alamat'] + "','"
				+ cd['no_telepon'] + "','"
				+ cd['jenis_kelamin'] + "')"
			)
			cursor.execute(
				"INSERT INTO STAF VALUES('"
				+ cd['email'] + "','"
				+ cd['npwp'] + "','"
				+ cd ['no_rekening']+ "','"
				+ cd['nama_bank'] + "','"
				+ cd['kantor_cabang'] + "')"
			)
		except Exception as e:
			self.add_error(None, e)

class RegisterCourierForm(forms.Form):
	email = forms.EmailField(max_length=50, required=True)
	password = forms.CharField(max_length=50, required=True, widget=forms.PasswordInput)
	nama_lengkap = forms.CharField(max_length=50, required=True)
	no_telepon = forms.CharField(max_length=20, required=True)
	jenis_kelamin = forms.CharField(max_length=1, required=True)
	alamat = forms.CharField(required=True)
	npwp = forms.CharField(max_length=20, required=True)
	no_rekening = forms.CharField(max_length=20, required=True)
	nama_bank = forms.CharField(max_length=50, required=True)
	kantor_cabang = forms.CharField(max_length=50, required=True)
	nomor_sim = forms.CharField(max_length=20, required=True)
	nomor_kendaraan = forms.CharField(max_length=20, required=True)
	jenis_kendaraan = forms.CharField(max_length=15, required=True)

	def save(self):
		cd = self.cleaned_data

		cursor = connection.cursor()

		try:
			cursor.execute(
				"INSERT INTO PENGGUNA VALUES('"
				+ cd['email'] + "','"
				+ cd['password'] + "','"
				+ cd['nama_lengkap']+ "','"
				+ cd['alamat'] + "','"
				+ cd['no_telepon'] + "','"
				+ cd['jenis_kelamin'] + "')"
			)
			cursor.execute(
				"INSERT INTO KURIR VALUES('"
				+ cd['email'] + "','"
				+ cd['npwp'] + "','"
				+ cd ['no_rekening']+ "','"
				+ cd['nama_bank'] + "','"
				+ cd['kantor_cabang'] + "','"
				+ cd['nomor_sim'] + "','"
				+ cd['nomor_kendaraan'] + "','"
				+ cd['jenis_kendaraan'] + "')"
			)
		except Exception as e:
			self.add_error(None, e)