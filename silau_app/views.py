from django.shortcuts import render
from django.shortcuts import redirect
from django.db import connection
from .forms import *
from django.core import serializers

# Create your views here.


def index(req):
    return render(req, "silau/index.html")


def register_staff(req):
    return render(req, "registration/register_staff.html")


def register_courier(req):
    return render(req, "registration/register_courier.html")


def register_customer(req):
    return render(req, "registration/register_customer.html")

# Login2


class User:
    def __init__(self, email, nama_lengkap, alamat, no_hp, jenis_kelamin, is_pelanggan, is_staf, is_kurir):
        self.email = email
        self.nama_lengkap = nama_lengkap
        self.alamat = alamat
        self.no_hp = no_hp
        self.jenis_kelamin = jenis_kelamin
        # Identification
        self.is_pelanggan = is_pelanggan
        self.is_staf = is_staf
        self.is_kurir = is_kurir

        self.no_virtual_account = None

        self.npwp = None
        self.no_rekening = None
        self.nama_bank = None
        self.kantor_cabang = None

        self.nomor_sim = None
        self.nomor_kendaraan = None
        self.jenis_kendaraan = None

    def setPelanggan(self, no_virtual_account):
        self.no_virtual_account

    def setStaf(self, npwp, no_rekening, nama_bank, kantor_cabang):
        self.npwp = npwp
        self.no_rekening = no_rekening
        self.nama_bank = nama_bank
        self.kantor_cabang = kantor_cabang

    def setKurir(self, nomor_sim, nomor_kendaraan, jenis_kendaraan):
        # setStaf(npwp, no_rekening, nama_bank, kantor_cabang)
        self.nomor_sim = nomor_sim
        self.nomor_kendaraan = nomor_kendaraan
        self.jenis_kendaraan = jenis_kendaraan

    def getJson(self):
        return {
            'email': self.email,
            'nama_lengkap': self.nama_lengkap,
            'alamat': self.alamat,
            'no_hp': self.no_hp,
            'jenis_kelamin': self.jenis_kelamin,
            'is_pelanggan': self.is_pelanggan,
            'is_staf': self.is_staf,
            'is_kurir': self.is_kurir,
            'no_virtual_account': self.no_virtual_account,
            'npwp': self.npwp,
            'self.no_rekening': self.no_rekening,
            'nama_bank': self.nama_bank,
            'kantor_cabang': self.kantor_cabang,
            'nomor_sim': self.nomor_sim,
            'nomor_kendaraan': self.nomor_kendaraan,
            'jenis_kendaraan': self.jenis_kendaraan
        }


def setLoginSession(request, form):
    user = None

    cd = form.cleaned_data
    cursor = connection.cursor()

    cursor.execute(
        "SELECT * FROM PENGGUNA NATURAL JOIN PELANGGAN WHERE email = '" +
        cd['email'] + "' AND password = '" + cd['password'] + "'"
    )
    row = cursor.fetchone()
    if row:
        user = User(row[0], row[2], row[3], row[4], row[5], True, False, False)
        user.setPelanggan(row[6])

    if not row:
        cursor.execute(
            "SELECT * FROM PENGGUNA NATURAL JOIN STAF WHERE email = '" +
            cd['email'] + "' AND password = '" + cd['password'] + "'"
        )
        row = cursor.fetchone()
        if row:
            user = User(row[0], row[2], row[3], row[4],
                        row[5], False, True, False)
            user.setStaf(row[6], row[7], row[8], row[9])

    if not row:
        cursor.execute(
            "SELECT * FROM PENGGUNA NATURAL JOIN KURIR WHERE email = '" +
            cd['email'] + "' AND password = '" + cd['password'] + "'"
        )
        row = cursor.fetchone()
        if row:
            user = User(row[0], row[2], row[3], row[4],
                        row[5], False, False, True)
            user.setStaf(row[6], row[7], row[8], row[9])
            user.setKurir(row[10], row[11], row[12])

    if user:
        request.session['pengguna'] = user.getJson()
    else:
        form.add_error(None, 'Email atau password anda salah')


def login(request):
    response = {}

    if(request.method == 'POST'):
        form = LoginForm(request.POST)
        if(form.is_valid()):
            setLoginSession(request, form)
            if not form.errors:
                return redirect('/')
    else:
        form = LoginForm()

    response['input_form'] = form
    return render(request, 'registration/login2.html', response)


def logout(request):
    request.session.clear()
    return redirect('/')


def register_staff(request):
    if(request.method == 'POST'):
        form = RegisterStaffForm(request.POST)
        if(form.is_valid()):
            form.save()
            if not form.errors:
                setLoginSession(request, form)
                if not form.errors:
                    return redirect('/')
    else:
        form = RegisterStaffForm()

    response = {
        'input_form': form
    }

    return render(request, "registration/register_staff2.html", response)
    
def register_courier(request):
    if(request.method == 'POST'):
        form = RegisterCourierForm(request.POST)
        if(form.is_valid()):
            form.save()
            if not form.errors:
                setLoginSession(request, form)
                if not form.errors:
                    return redirect('/')
    else:
        form = RegisterCourierForm()

    response = {
        'input_form': form
    }

    return render(request, "registration/register_courier2.html", response)
