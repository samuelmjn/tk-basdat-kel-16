from django.urls import path
from .views import *

app_name = 'silau'

urlpatterns = [
    path('', index, name='index'),
    path('login2/', login, name='login2'),
    path('logout2/', logout, name='logout2'),
    path('register/staff2/', register_staff, name='register_staff2'),
    path('register/courier2/', register_courier, name='register_courier2'),
    path('register/staff/', register_staff, name='register_staff'),
    path('register/courier/', register_courier, name='register_staff'),
    path('register/customer/', register_customer, name='register_customer')
]
