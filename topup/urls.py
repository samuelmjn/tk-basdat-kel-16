from django.urls import path
from .views import topup, update, create, delete

app_name = 'topup'

urlpatterns = [
    path('', topup, name='topup-index'),
    path('update/', update, name='topup-update'),
    path('create/', create, name='topup-create'),
    path('delete/', delete, name='topup-delete')
]
