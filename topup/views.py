from datetime import datetime

from django.db import connection
from django.shortcuts import render, redirect


# Create your views here.
def topup(req):
    is_authenticated = False
    try:
        pengguna = req.session['pengguna']
        is_authenticated = True
    except KeyError:
        return render(req, 'topup/index.html', {'is_authenticated': is_authenticated})

    if pengguna['is_pelanggan']:
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM transaksi_topup_dpay WHERE email='{}'".format(pengguna['email']))
            columns = [col[0] for col in cursor.description]
            data = [
                dict(zip(columns, row))
                for row in cursor.fetchall()
            ]
    else:
        with connection.cursor() as cursor:
            cursor.execute('SELECT * FROM transaksi_topup_dpay')
            columns = [col[0] for col in cursor.description]
            data = [
                dict(zip(columns, row))
                for row in cursor.fetchall()
            ]

            for d in data:
                d['diff'] = int((datetime.now() - d['tanggal']).total_seconds()/ 60.0)

    ctx = {'data': data, 'pengguna': pengguna, 'is_authenticated': is_authenticated}
    return render(req, 'topup/index.html', ctx)


def update(req):
    if req.method == 'POST':
        nominal = req.POST.get('nominal')
        email = req.POST.get('email')
        tanggal = req.POST.get('tanggal')
        with connection.cursor() as cursor:
            cursor.execute(
                "UPDATE transaksi_topup_dpay SET nominal='{}' WHERE email='{}' AND tanggal='{}'".format(nominal, email,
                                                                                                        tanggal))
            return redirect('/topup')

    email = req.GET.get('email')
    tanggal = req.GET.get('tanggal')
    nominal = req.GET.get('nominal')
    diff = req.GET.get('diff')
    ctx = {'email': email, 'tanggal': tanggal, 'nominal': nominal, 'diff': diff}
    return render(req, 'topup/update.html', ctx)


def create(req):
    if req.method == 'POST':
        nominal = req.POST.get('nominal')
        email = req.POST.get('email')
        with connection.cursor() as cursor:
            cursor.execute("INSERT INTO transaksi_topup_dpay(email, nominal, tanggal) VALUES('{}', '{}', '{}')".format(email, nominal, datetime.now()))
        return redirect('/topup')
    with connection.cursor() as cursor:
        cursor.execute('SELECT * FROM pelanggan')
        columns = [col[0] for col in cursor.description]
        data = [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]
    ctx = {'pelanggan': data}
    return render(req, 'topup/create.html', ctx)


def delete(req):
    nominal = req.GET.get('nominal')
    email = req.GET.get('email')
    tanggal = req.GET.get('tanggal')
    with connection.cursor() as cursor:
        cursor.execute("DELETE FROM transaksi_topup_dpay WHERE email='{}' AND tanggal='{}' AND nominal='{}'".format(email, tanggal, nominal))
        return redirect('/topup')
