from django.urls import path
from .views import index, create_daftar, read_item, create_item, data_daftar_laundry, data_item, delete_daftar_laundry, update_daftar_laundry, create_item_post, find_trx_date_by_email

app_name = 'laundry'

urlpatterns = [
    path('daftar/', index, name='daftar-laundry-rud'),
    path('daftar/data', data_daftar_laundry, name='daftar-laundry-data'),
    path('daftar/create', create_daftar, name='daftar-laundry-create'),
    path('daftar/delete', delete_daftar_laundry, name='daftar-laundry-delete'),
    path('daftar/update', update_daftar_laundry, name='daftar-laundry-update'),
    path('daftar/trx', find_trx_date_by_email, name='daftar-laundry-find-trx-by-email'),
    path('item/', read_item, name='read-item'),
    path('item/data', data_item, name='item-data'),
    path('item/create', create_item, name='create-item'),
    path('item/create/post', create_item_post, name='create-item-post')
]
