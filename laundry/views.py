from datetime import datetime

from django.shortcuts import render, redirect
from django.db import connection
from django.http import JsonResponse, HttpResponseForbidden, HttpResponseBadRequest
from django.views.decorators.csrf import csrf_exempt
import json


# Create your views here.
def index(request):
    return render(request, "laundry/rud-daftar-laundry.html")


def create_daftar(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        jumlah_item = request.POST.get('jumlahItem')
        kodeitem = request.POST.get('kodeitem')
        tanggal = request.POST.get('tanggal')
        with connection.cursor() as cursor:
            cursor.execute(
                "INSERT INTO daftar_laundry VALUES ('{}', '{}', (SELECT MAX(no_urut) + 1 FROM daftar_laundry), '{}', '{}')".format(
                    email, tanggal, jumlah_item, kodeitem))
        return redirect('/laundry/daftar')

    with connection.cursor() as cursor:
        cursor.execute('SELECT * FROM pelanggan')
        columns = [col[0] for col in cursor.description]
        data = [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]

        cursor.execute('SELECT * FROM item')
        columns = [col[0] for col in cursor.description]
        items = [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]
    ctx = {'data': data, 'items': items}
    return render(request, "laundry/create-daftar-laundry.html", ctx)


def read_item(request):
    return render(request, "laundry/read-item.html")


def create_item(request):
    return render(request, "laundry/create-item.html")


def data_daftar_laundry(request):
    pengguna = request.session['pengguna']
    if pengguna and pengguna['is_pelanggan']:
        with connection.cursor() as cursor:
            cursor.execute('SELECT * FROM daftar_laundry WHERE email = %s', [pengguna['email']])
            columns = [col[0] for col in cursor.description]
            d = [
                dict(zip(columns, row))
                for row in cursor.fetchall()
            ]
        return JsonResponse({'data': d})
    elif pengguna and pengguna['is_staf']:
        with connection.cursor() as cursor:
            cursor.execute('SELECT * FROM daftar_laundry')
            columns = [col[0] for col in cursor.description]
            d = [
                dict(zip(columns, row))
                for row in cursor.fetchall()
            ]
        return JsonResponse({'data': d})
    else:
        return HttpResponseForbidden()


@csrf_exempt
def delete_daftar_laundry(request):
    if request.method == 'POST':
        d = json.loads(request.body)
        print(d)
        try:
            with connection.cursor() as cursor:
                cursor.execute('DELETE FROM daftar_laundry WHERE email=%s AND tanggal_transaksi=%s AND no_urut=%s',
                               [d['email'], d['tanggal'], d['nourut']])
                return JsonResponse({'success': True})
        except Exception as e:
            print(e)
            return JsonResponse({'success': False})


@csrf_exempt
def update_daftar_laundry(request):
    if request.method == 'POST':
        d = json.loads(request.body)
        print(d)
        try:
            with connection.cursor() as cursor:
                cursor.execute(
                    'UPDATE daftar_laundry SET jumlah_item=%s, kode_item=%s WHERE email=%s AND tanggal_transaksi=%s AND no_urut=%s',
                    [d['nitem'], d['kodeitem'], d['email'], d['tanggal'], d['nourut']])
                return JsonResponse({'success': True})
        except Exception as e:
            print(e)
            return JsonResponse({'success': False})


def data_item(request):
    pengguna = request.session['pengguna']
    if pengguna:
        with connection.cursor() as cursor:
            cursor.execute('SELECT * FROM item')
            columns = [col[0] for col in cursor.description]
            d = [
                dict(zip(columns, row))
                for row in cursor.fetchall()
            ]
        return JsonResponse({'data': d})
    else:
        return HttpResponseForbidden()


@csrf_exempt
def create_item_post(request):
    if request.method == 'POST':
        d = json.loads(request.body)
        print(d)
        try:
            with connection.cursor() as cursor:
                cursor.execute('INSERT INTO item VALUES(%s, %s)', [d['kode'], d['nama']])
                return JsonResponse({'success': True})
        except Exception as e:
            print(e)
            return JsonResponse({'success': False})


@csrf_exempt
def find_trx_date_by_email(req):
    if req.method == 'POST':
        b = json.loads(req.body)
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM transaksi_laundry WHERE email='{}'".format(b['email']))
            columns = [col[0] for col in cursor.description]
            data = [
                dict(zip(columns, row))
                for row in cursor.fetchall()
            ]

            return JsonResponse({'data': data})
